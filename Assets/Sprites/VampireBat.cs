﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VampireBat : MonoBehaviour
{
    public GameObject player;
    public float speed;
    public float minRange;
    public float maxRange; 
    
    // Use this for initialization
    void Start()
    {
        player = GameObject.FindObjectOfType<Player>().gameObject; 
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, (speed * Time.deltaTime));
    }
}


  