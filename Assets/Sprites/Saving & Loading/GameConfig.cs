﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using UnityEditor;

/*[CustomEditor(typeof(GameConfig))]
public class GameConfigEditor : Editor
{

    public override void OnInspectorGUI()
    {
        GameConfig myScript = (GameConfig)target;
        if (GUILayout.Button("Print File Location"))
        {
            myScript.PrintFileLoc();
        }
        DrawDefaultInspector();

    }
}*/

[CreateAssetMenu(fileName = "GameConfig", menuName = "Config/Game", order = 1)]
public class GameConfig : ScriptableObject
{
 
    public List<bool> savedBools = new List<bool>();
    public List<int> savedIntergers = new List<int>();

    public void Save()
    {
        // convert this object to JSON
        string jsonOutput = JsonUtility.ToJson(this);

        // write out the data
        StreamWriter sw = new StreamWriter(FilePath);
        sw.Write(jsonOutput);
        sw.Close();
    }

    public string FilePath
    {
        get
        {
            return Path.Combine(Application.persistentDataPath, this + ".json");
        }
    }

    public void PrintFileLoc()
    {
        if (File.Exists(FilePath))
            Debug.Log("File Loaction = " + FilePath);
        else
            Debug.LogWarning("FilePath dosn't exist");

    }

    public void Load()
    {
        // read in the data (if present)
        if (File.Exists(FilePath))
        {
            // read the contents of the file
            StreamReader sr = new StreamReader(FilePath);
            string jsonInput = sr.ReadToEnd();
            sr.Close();

            // update this object
            JsonUtility.FromJsonOverwrite(jsonInput, this);
        }
    }
}