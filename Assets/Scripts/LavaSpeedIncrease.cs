﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaSpeedIncrease : MonoBehaviour
{
    public TileSpawner ts;
    public float rngBlank, rngRed, speedIncrease;

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            GameObject.FindObjectOfType<Lava>().IncreaseSpeed(speedIncrease);
            ts.rng1 = rngBlank;
            ts.rng2 = rngRed;
            Destroy(gameObject);
        }
    }
}
