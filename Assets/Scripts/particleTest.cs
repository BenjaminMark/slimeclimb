﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class particleTest : MonoBehaviour
{
    
    public ParticleSystem fireBall; 

	// Use this for initialization
	void Start ()
    {
        fireBall = gameObject.GetComponent<ParticleSystem>(); 
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            fireBall.Play();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            fireBall.Stop();
        }


    }
}
