﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tilegoldenPath : MonoBehaviour
{
    public Animator tileAnim; 

    // Use this for initialization
    void Start()
    {
        tileAnim = GetComponent<Animator>();
        tileAnim.SetTrigger("goldenPath"); 
    }

    // Update is called once per frame
    void Update()
    {

    }
}
