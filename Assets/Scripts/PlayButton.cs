﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButton : MonoBehaviour
{

    public Buttons buttons;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadSceneOnEndOfAnim()
    {
        buttons.CanPlay(true);
        buttons.Load(1);
    }
}
