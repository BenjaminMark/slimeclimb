﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Tile : MonoBehaviour
{
    //just added this for testing 
    //public GameObject gameOverPanel; 
    [System.Serializable]
    public class CollectablesData
    {
        [Tooltip("items name that also changes the element name")]
        public string name;

        [Space(5)]
        [Header("Chance to spawn"), Tooltip("Random roll gap for the rng to land on between 0 and 1")]
        public float minChance;
        public float maxChance;

        [Space(5)]
        [Tooltip("Prefab gameobject you want to spawn on the tile as a visual for the player")]
        public GameObject itemPrefab;
        [Tooltip("Items sprite render color. change to alter color of item")]
        public Color itemColor = Color.white;

        [Space(5)]
        [Tooltip("position offset the item spawns from its tile")]
        public Vector2 spawnOffset = new Vector2(2.8f, 4.4f);
        [Tooltip("rotation the item spawns at")]
        public Quaternion spawnRotation;

        // public int timesSpawned;

        //[Tooltip("Chance for tile to contain this object")]
        // public float Empty,
        //     ChestKey,
        //     Coin,
        //     JumpGuider,
        //     MegaJump,
        //     CoinRain,
        //     SuperSlime;
    }

    public enum TileTypes
    {
        OriginTile,
        StandardTile,
        BadTile,
        BlankTile
    }

    [Header("Tile Info:")]
    [Tooltip("Tile type the tile is assigned to be at a core. Ie, badtype = death on enter")]
    public TileTypes tileType = TileTypes.OriginTile;

    public enum SubTileTypes
    {
        Empty,
        ChestKey,
        Coin,
        JumpGuider,
        MegaJump,
        CoinRain,
        SuperSlime
    }

    [Tooltip("Tile type the tile is assigned to contain. Ie, power ups, keys etc.")]
    public SubTileTypes collectableType = SubTileTypes.Empty;

    Player player;
    GameManager gameMan;
    TileSpawner tileSpawn;
    //Missions missionMan;
    //bool canMissionProgress = false;
    public GameObject coinPrefab,
        anim;
    Vector2 offset = new Vector2(0f, 0.5f);
    public SpriteRenderer spriteRend;
    public Sprite goodTile,
        badTile;
    public bool hasSpawnedRow = false;

    [Tooltip("Distance in which the tile hides its evilness from the player")]
    public float distanceToHide = 3f;
    public float distanceToSwap = 2f;
    public float colorTransitionSpeed;
    private float removeTileDistance = -7;
    Tile parentTile;

    public List<CollectablesData> collectableData = new List<CollectablesData>();

    // Use this for initialization
    //
    void OnEnable()
    {
        // spriteRend = gameObject.GetComponent<SpriteRenderer>();
        gameMan = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        tileSpawn = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<TileSpawner>();
        //missionMan = GameObject.FindGameObjectWithTag("MissionManager").GetComponent<Missions>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        thingo();

        //for sample testing 
        //for (int i = 0; i < 100; i++)
        //{
        //    AssignSubTile();
        //}
        // for over time testing
        //InvokeRepeating("AssignSubTile", 1, 2);
    }
    /// <summary>
    /// HACK: change later lolloololo
    /// </summary>
    public void thingo()
    {
        if (tileType != TileTypes.OriginTile)
            AssignTileType(false, 0, 0);
    }

    public void AssignTileType(bool rollCheck, float rng1, float rng2)
    {
        if (rollCheck)
        {
            int rng = UnityEngine.Random.Range(0, 100);
            if (rng <= rng1)
            {
                tileType = TileTypes.BlankTile;
                gameObject.tag = "BlankTile";
            }
            else if (rng <= rng2)
            {
                tileType = TileTypes.BadTile;
                gameObject.tag = "BadTile";
            }
            else
            {
                tileType = TileTypes.StandardTile;
                gameObject.tag = "GoodTile";
            }
        }

        switch (tileType)
        {
            case TileTypes.OriginTile:
                // spriteRend.sprite = goodTile;
                gameObject.tag = "GoodTile";
                break;
            case TileTypes.StandardTile:
                //spriteRend.sprite = goodTile;
                gameObject.tag = "GoodTile";
                //gameObject.GetComponent<Animator>().SetTrigger("Reset");
                AssignSubTile();
                break;
            case TileTypes.BadTile:
                //spriteRend.color = Color.red;
                //spriteRend.sprite = badTile;
                gameObject.GetComponent<Animator>().SetTrigger("BadTile");

                break;
            case TileTypes.BlankTile:
                //spriteRend.color = Color.clear;
                gameObject.tag = "BlankTile";
                gameObject.GetComponent<Animator>().SetTrigger("BlankTile");

                break;
            default:
                break;
        }
    }

    private void AssignSubTile()
    {
        //float rngspawn = UnityEngine.Random.Range(0f, 1f);
        double rngspawn = System.Math.Round(UnityEngine.Random.Range(0f, 1f), 2);
        if (rngspawn < 0.1f)
        {
            Instantiate(coinPrefab, (Vector2)transform.position + offset, transform.rotation);
            //float rng = UnityEngine.Random.Range(0f, 1f);

            /* for (int i = 0; i < collectableData.Count; i++)
             {
                 if (rng > collectableData[i].minChance && rng <= collectableData[i].maxChance)
                 {
                     //Debug.Log("~~!! _Item: " + collectableData[i].name + " has spawned_ !!~~");
                     GameObject newCollectable = Instantiate(collectableData[i].itemPrefab, (Vector2)transform.position + collectableData[i].spawnOffset, collectableData[i].spawnRotation);
                     newCollectable.GetComponent<SpriteRenderer>().color = collectableData[i].itemColor;
                     newCollectable.name = collectableData[i].name;
                     //     Destroy(newCollectable, 5);
                     //  subTileData[i].timesSpawned++;
                     break;
                 }
                 else
                 {
                     continue;
                 }
             }*/
            //yeeeettt
        }
        collectableType = SubTileTypes.Empty;
        //Debug.Log("No Item has spawned; Tile is blank");
    }

    void DiminishColor()
    {
        if (tileType == TileTypes.BadTile)
            spriteRend.color = Color.Lerp(gameObject.GetComponent<Renderer>().material.color, Color.white, colorTransitionSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.position.y - player.gameObject.transform.position.y <= removeTileDistance)
        {
            gameObject.GetComponent<Animator>().ResetTrigger("Reset");
            gameObject.GetComponent<Animator>().SetTrigger("Reset");
            gameObject.SetActive(false);
        }

        if (gameObject.transform.position.y - player.gameObject.transform.position.y > 8.75f)
        {
            gameObject.GetComponent<Animator>().ResetTrigger("Reset");
            gameObject.GetComponent<Animator>().SetTrigger("Reset");
            gameObject.SetActive(false);
        }

        if (tileType == TileTypes.BadTile && (transform.position.y - player.transform.position.y) <= distanceToHide)
        {
            Animator anim = gameObject.GetComponent<Animator>();
            if (anim.gameObject.activeSelf)
                anim.SetTrigger("Hide");
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Return out if what collided was NOT the player
        if (collision.CompareTag("Player"))
        {
            if (tileType == TileTypes.BadTile || tileType == TileTypes.BlankTile)
            {
                // gameOverPanel.SetActive(true); 
                //gameMan.canPlay = false;
                //StartCoroutine(player.Fall(0.075f));
                tileSpawn.DeleteTiles();
                player.anim.speed = (player.speed * 0.5f);
                player.test.play = false;
                player.inputActive = false;
                player.anim.SetTrigger("Fall");
                player.loc += new Vector3(0, -2, 0);
                FindObjectOfType<AudioManager>().Play("playerMiss");
                if (tileType == TileTypes.BadTile)
                {
                    gameObject.GetComponent<Animator>().SetTrigger("BreakTile");
                }

                //if (player.failCount >= 2)
                //StartCoroutine(RunFail());

                //gameMan.Fail();
            }
            else
            {
                gameObject.GetComponent<Animator>().SetTrigger("LightUp");
                gameMan.ScoreUpdate();
                if (player.inputActive == true)
                    player.failCount = 0;
                tileSpawn.tileLoc = gameObject.transform;
                tileSpawn.SpawnTiles(16, 6, 16);
            }
        }
    }

    IEnumerator RunFail()
    {
        yield return new WaitForSeconds(1);
        gameMan.Fail();
    }
}
