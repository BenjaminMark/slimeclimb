using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Monetization;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    #region SaveLoading Code
    [SerializeField] private GameConfig ReferenceConfig; // this links to the SO

    public GameConfig ActiveConfig;


    public void FeedDataAndSave()
    {
        ActiveConfig.savedIntergers.Clear();

        ActiveConfig.savedIntergers.Add(highScore);
        ActiveConfig.savedIntergers.Add(currency);
        ActiveConfig.savedIntergers.Add(heightMission);
        ActiveConfig.savedIntergers.Add(coinMission);
        ActiveConfig.savedIntergers.Add(boostMission);
       // ActiveConfig.savedIntergers.Add(cosmeticMission);
        ActiveConfig.savedIntergers.Add(heightProgress);
        ActiveConfig.savedIntergers.Add(coinProgress);
        ActiveConfig.savedIntergers.Add(boostProgress);
       // ActiveConfig.savedIntergers.Add(cosmeticProgress);
        ActiveConfig.savedIntergers.Add(costumeSelection);
        ActiveConfig.savedIntergers.Add(colourSelection);

        ActiveConfig.Save();
    }
    /// <summary>
    /// This is so bad but neeed to get this workinggsljfhaslkjdfhalkjs
    /// </summary>
    void LoadAndPullData()
    {
        ActiveConfig.Load();
        if (ActiveConfig.savedIntergers.Count > 0)
        {
            int index = 0;
            highScore = ActiveConfig.savedIntergers[index];
            ++index;
            currency = ActiveConfig.savedIntergers[index];
            ++index;
            heightMission = ActiveConfig.savedIntergers[index];
            ++index;
            coinMission = ActiveConfig.savedIntergers[index];
            ++index;
            boostMission = ActiveConfig.savedIntergers[index];
            ++index;
           // cosmeticMission = ActiveConfig.savedIntergers[index];
          //  ++index;
            heightProgress = ActiveConfig.savedIntergers[index];
            ++index;
            coinProgress = ActiveConfig.savedIntergers[index];
            ++index;
            boostProgress = ActiveConfig.savedIntergers[index];
            ++index;
           // cosmeticProgress = ActiveConfig.savedIntergers[index];
           // ++index;
            costumeSelection = ActiveConfig.savedIntergers[index];
            ++index;
            colourSelection = ActiveConfig.savedIntergers[index];
            ++index;

        }
    }

    #endregion

    public bool canPlay = true,
        playingAd = false;

    public int score,
        currency, highScore;

    private int currentCurrency = 0;
    private int totalRuns = 0;

    public GameObject cosEffect;

    //UI
    public UI ui;
    public int chestKeys;
    //MissionInfo
    Missions missionMan;
    public int heightMission,
        coinMission,
        boostMission,
        //will be removing cosmetic mission stuff
        cosmeticMission,
        heightProgress,
        coinProgress,
        boostProgress,
        //this too
        cosmeticProgress;
    public bool canProgressCoinMission = true,
       canProgressHeightMission = true,
       canProgressBoostMission = true;
    //also removing this
    public bool canProgressCosmeticMission = false;

    //CosmeticsInfo
    public int costumeSelection,
        colourSelection;

    //PlayerInfo
    public List<Sprite> playerSprites = new List<Sprite>();
    public List<Color> playerColors = new List<Color>();
    public Sprite slimeColour;
    public Cosmetics cosmetics;


    private void Awake()
    {
        //Initiates the round tracker, so the system  can determine whether the player is to be shown adds

        DontDestroyOnLoad(this.gameObject);
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
            return;
        }
        // clone the reference config so any changes won't overwrite
        ActiveConfig = ScriptableObject.Instantiate(ReferenceConfig);
        ActiveConfig.Load();
        LoadPlayerPrefs();
        Analytics.GameStart(true);
        AdController.SetFirstGameFlag();
        // initialise the monetisation
        Monetization.Initialize(AdCommon.GameId, true);
    }

    public void Start()
    {
        cosmetics = GameObject.FindGameObjectWithTag("CosmeticsManager").GetComponent<Cosmetics>();

        CurrentCurrency();
        LoadAndPullData();
    }

    public void CurrentCurrency()
    {
        currentCurrency = currency;
    }

    public void Fail()
    {
        Analytics.RoundEnd(score, currency - currentCurrency);
        AdController.SetRoundEndTime();

        StartCoroutine(FailAd());

    }

    bool NewHighScore()
    {
        if (Mathf.Max(score, highScore) == highScore)
        {
            return false;
        }
        else if (Mathf.Max(score, highScore) == score)
        {
            return true;
        }

        throw new System.Exception();
    }

    public void ScoreUpdate()
    {
        score++;
        if (!ui)
        {
            GetUIMan();
        }

        string scoreText = score.ToString();
        ui.UpdateGameUI(scoreText, ui.currentScore);

        if (score >= highScore)
        {
            highScore = score;
            string highScoreText = highScore.ToString();
            ui.UpdateGameUI(highScoreText, ui.highscore);
        }

        if (!missionMan)
        {
            GetMissionMan();
        }

        if (canProgressHeightMission == true)
        {
            missionMan.UpdateMissionProgress(ref missionMan.HeightMissions[missionMan.currentHeightMission].progress,
                missionMan.HeightMissions[missionMan.currentHeightMission].goal,
                ref missionMan.currentHeightMission,
                missionMan.HeightMissions,
                ref canProgressHeightMission);
            heightProgress++;
        }
        
    }

    public void CoinPickUp()
    {
        currency++;
        if (!ui)
        {
            GetUIMan();
        }

        string currencyText = currency.ToString();
        ui.UpdateGameUI(currencyText, ui.currentCoins);

        if (!missionMan)
        {
            GetMissionMan();
        }

        if (canProgressCoinMission == true)
        {
            missionMan.UpdateMissionProgress(ref missionMan.CoinMissions[missionMan.currentCoinMission].progress,
                missionMan.CoinMissions[missionMan.currentCoinMission].goal,
                ref missionMan.currentCoinMission,
                missionMan.CoinMissions,
                ref canProgressCoinMission);
            coinProgress = missionMan.CoinMissions[missionMan.currentCoinMission].progress;
        }
    }

    public void BoostPickUp()
    {
        if (!missionMan)
        {
            GetMissionMan();
        }

        if (canProgressBoostMission == true)
        {
            missionMan.UpdateMissionProgress(ref missionMan.BoostMissions[missionMan.currentBoostMission].progress,
                missionMan.BoostMissions[missionMan.currentBoostMission].goal,
                ref missionMan.currentBoostMission,
                missionMan.BoostMissions,
                ref canProgressBoostMission);
            heightProgress = missionMan.BoostMissions[missionMan.currentBoostMission].progress;
        }
        else
        {
            heightProgress = 0;
        }
    }

    public void GetPlayerCosmetics()
    {
        playerSprites.Clear();
        playerColors.Clear();

        Player playerScr = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        slimeColour = player.GetComponentInChildren<SpriteRenderer>().sprite;
        foreach (SpriteRenderer sprite in playerScr.cosmetics)
        {
            playerSprites.Add(sprite.sprite);
            playerColors.Add(sprite.color);
        }
    }

    public void ReplacePlayerCosmetics()
    {
        cosmetics.currentSelectionCostume = costumeSelection;
        Player playerScr = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        if (slimeColour == null)
        {
            foreach (Cosmetics.BasicColourData colour in cosmetics.ActiveConfig.basicColour)
            {
                if (colour.isActive)
                {
                    slimeColour = colour.newColour;
                    break;
                }
            }
        }
        if (slimeColour != null)
        {
            GameObject.FindGameObjectWithTag("PlayerSprite").GetComponent<SpriteRenderer>().sprite = slimeColour;
        }

        cosmetics.UpdateCostume();
        
        for (int i = 0; i < playerSprites.Count; i++)
        {
            playerScr.cosmetics[i].sprite = playerSprites[i];
            playerScr.cosmetics[i].color = playerColors[i];
            if (playerScr.cosmetics[i].color == Color.black)
            {
                playerScr.cosmetics[i].color = Color.clear;
            }
            else
            {
                ActivateCosmeticEffect();
                if (cosEffect)
                    Destroy(cosEffect);
                cosEffect = null;
            }
            }

    }

    public void ActivateCosmeticEffect()
    {
        if (cosmetics.ActiveConfig.cosmeticsets[cosmetics.currentSelectionCostume].cosmeticEffectActive == true && canPlay == true)
        {
            GameObject effectHolder = GameObject.FindGameObjectWithTag("CosEffect");
            cosEffect = Instantiate(cosmetics.ActiveConfig.cosmeticsets[cosmetics.currentSelectionCostume].cosmeticEffect,
                effectHolder.transform);
        }
        else
            return;
    }

    public void GetMissionMan()
    {
        missionMan = GameObject.FindGameObjectWithTag("MissionManager").GetComponent<Missions>();
    }

    public void GetUIMan()
    {
        ui = GameObject.FindGameObjectWithTag("UI").GetComponent<UI>();
    }

    public IEnumerator FailAd()
    {
        FeedDataAndSave();

        //If an Advertisement Should be played this round
        if (AdController.CanPlayAd(totalRuns))
        {
            // monetisation is not ready?
            if (!Monetization.IsReady(AdCommon.FailAdPlacementId))
            {
                // if the monetisation isn't ready then treat it as a fail
                HandleFailAdShowResult(ShowResult.Failed);
            }
            else
            {
                //Setup the ad options - link to callback that gets run in response to ad ending
                //NOTE I changed this, so it sets the finishCallback in the new creation.
                ShowAdCallbacks options = new ShowAdCallbacks
                {
                    finishCallback = HandleFailAdShowResult
                };

                // show the ad
                playingAd = true;
                SceneManager.LoadScene(1);
                canPlay = false;
                StartCoroutine(FailAdWrapUp());
                ShowAdPlacementContent ad = Monetization.GetPlacementContent(AdCommon.FailAdPlacementId) as ShowAdPlacementContent;
                ad.Show(options);
            }
        }
        else
        {
            //Ad shouldn't be played this round
            playingAd = false;
            SceneManager.LoadScene(1);
            canPlay = false;
            StartCoroutine(FailAdWrapUp());
        }
        yield return null;
    }

    void HandleFailAdShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            // Reward the player
            playingAd = false;
            SceneManager.LoadScene(1);
            canPlay = false;
            StartCoroutine(FailAdWrapUp());
        }
        else if (result == ShowResult.Skipped)
        {
            Debug.LogWarning("The player skipped the video - DO NOT REWARD!");
        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
        }

        SceneManager.LoadScene(1);
        canPlay = false;
        StartCoroutine(FailAdWrapUp());
    }

    public IEnumerator FailAdWrapUp()
    {
        yield return new WaitForSeconds(0.1f);

        if (!ui)
        {
            GetUIMan();
        }

        ui.Activate(ui.gameOverUI);
        for(int i =0; i < ui.gameUI.Count; i++)
        {
            ui.DeActivate(ui.gameUI[i]);
        }

        //ui.UpdateMissionUI(ui.HeightBar, heightProgress, missionMan.HeightMissions[missionMan.currentHeightMission].goal);
        //ui.UpdateMissionUI(ui.BoostBar, boostProgress, missionMan.BoostMissions[missionMan.currentBoostMission].goal);
        //if (canProgressCosmeticMission == true)
        //  ui.UpdateMissionUI(ui.CoinBar, cosmetics.cosmeticsets[cosmetics.currentSelectionCostume].missionProgress, cosmetics.cosmeticsets[cosmetics.currentSelectionCostume].pieceCost[3]);
        //else ui.UpdateMissionUI(ui.CoinBar, coinProgress, missionMan.CoinMissions[missionMan.currentCoinMission].goal);
        ui.UpdateGameOverUI();
        //will need to track this between scenes

        if (chestKeys > 0)
        {
            ui.Activate(ui.chest);
        }

        Analytics.GameFailed(score);

        if (NewHighScore())
        {
            highScore = score;
        }
    }

    public void OnApplicationQuit()
    {
        FeedDataAndSave();
        Analytics.GameQuit(Time.time, totalRuns);
    }
    public void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            PlayerPrefs.SetFloat(Analytics.timePlayedKey, Time.time);
            PlayerPrefs.SetInt(Analytics.runsPlayedKey, totalRuns);
            PlayerPrefs.SetInt(Analytics.storedAnalytics, 1);
            //Analytics.GamePaused(Time.time, totalRuns);
            //ui.GetComponent<Buttons>().Pause();
        }
        //else if (!pause)
        //{
        //    //totalRuns = 0; 
        //}
    }
    public void AddRun()
    {
        totalRuns += 1;
    }
    public void AnalyticsDataCleanup()
    {
        if (PlayerPrefs.GetInt(Analytics.storedAnalytics) == 1)
        {
            Analytics.GamePaused(PlayerPrefs.GetFloat(Analytics.timePlayedKey), PlayerPrefs.GetInt(Analytics.runsPlayedKey));

            PlayerPrefs.SetInt(Analytics.storedAnalytics, 0);
            PlayerPrefs.SetFloat(Analytics.timePlayedKey, 0);
            PlayerPrefs.SetInt(Analytics.runsPlayedKey, 0);
        }
    }
    /// <summary>
    /// Needs to be run in Game Manager Awake function.
    /// </summary>
    public void LoadPlayerPrefs()
    {
        //HACK FIX should be a variable
        //The string should be linked to AdTacker.isFirstGame
        if (!PlayerPrefs.HasKey("isFirstGame"))
        {
            PlayerPrefs.SetInt("isFirstGame", 1);
        }
    }
}
