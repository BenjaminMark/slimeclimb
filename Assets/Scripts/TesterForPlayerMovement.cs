﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TesterForPlayerMovement : MonoBehaviour
{

    public Vector3 loc = new Vector3(0, 0, 0);

    public GameObject player;

    public Player playerSCR;

    public float speed,
        step;

    public bool move,
        play;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (play == true)
            playerSCR.inputActive = true;
        else
            playerSCR.inputActive = false;
    }
}

