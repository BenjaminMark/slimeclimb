﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq; 
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{/// <summary>
 /// "This script manages the audio for the game, there is a another script called soundscript that this inherits from" 
 /// </summary>
 

    
    [Tooltip("just add one to the list via the inspector and you'll see how it works")]
    public SoundScript[] sounds;

    // Use this for initialization
    void Awake()
    {

        foreach (SoundScript s in sounds)
        {            
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.loop = s.loop;
           
            // s.source.pitch = s.pitch; 
        }
    }

    public void Start()
    {
        FindObjectOfType<AudioManager>().Play("BGmusic");
    }

    public void Play(string name)
    {
        SoundScript s = Array.Find(sounds, sound => sound.name == name);
        if(s.randomisePitch == true)
        {
            s.pitch = UnityEngine.Random.Range(0.5f, 1f);
            s.source.pitch = s.pitch;
        }
        s.source.Play();
    }
}
