﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class Cosmetics : MonoBehaviour
{

    [SerializeField] public CosmeticConfig ReferenceConfig; // this links to the SO

    public CosmeticConfig ActiveConfig;

    [System.Serializable]
    public class CosmeticsData
    {
        [Tooltip("Cosmetic Name")]
        public string name;

        [Tooltip("Is this the current unlocked cosmetic?")]
        public bool isActive;

        [Tooltip("Is this a basic cosmetic?")]
        public bool basicCosmetic;

        [Space(5)]
        [Tooltip("Insert the desired sprites here")]
        [JsonIgnoreAttribute]
        public List<Sprite> CostumeSprites = new List<Sprite>();

        [Space(5)]
        [Tooltip("Is the costume piece unlocked?")]
        public List<bool> pieceUnlocked = new List<bool>();

        [Tooltip("Cost per piece and how much collectables required for 4th")]
        public List<int> pieceCost = new List<int>();

        /* [Tooltip("Is the mission active?")]
         public bool missionActive;

         [Tooltip("Current progress towards that goal")]
         public int missionProgress;

         [Tooltip("If cosmetic mission, put the new coin sprite here")]
         [JsonIgnoreAttribute]
         public Sprite cosmeticMissionCollectable;*/

        [Tooltip("Cosmetic effect active yes/no")]
        public bool cosmeticEffectActive;

        [Tooltip("Cosmetic effect object goes here")]
        [JsonIgnoreAttribute]
        public GameObject cosmeticEffect;
    }

    [System.Serializable]
    public class BasicColourData
    {
        [Tooltip("colour name goes here")]
        public string name;

        [Tooltip("Tick this if this is the active colour")]
        public bool isActive;

        [Tooltip("put the new colour sprite here")]
        [JsonIgnoreAttribute]
        public Sprite newColour;

        [Tooltip("this indicates whether this cosmetic is unlocked")]
        public bool isUnlocked;
    }

    //public List<BasicColourData> basicColour = new List<BasicColourData>();
    //public List<CosmeticsData> cosmeticsets = new List<CosmeticsData>();
    private GameManager gameMan;
    private Player player;
    public int currentSelectionCostume,
        currentSelectionColour;


    public List<BasicColourData> basicColour
    {
        get
        {
            return ActiveConfig.basicColour;
        }
    }

    public List<CosmeticsData> cosmeticsets
    {
        get
        {
            return ActiveConfig.cosmeticsets;
        }
    }

    // Use this for initialization
    void Start()
    {
        // clone the reference config so any changes won't overwrite
        ActiveConfig = ScriptableObject.Instantiate(ReferenceConfig);
        ActiveConfig.Load();

        //ReferenceConfig.cosmeticsets = new List<CosmeticsData>(cosmeticsets);
        //ReferenceConfig.basicColour = new List<BasicColourData>(basicColour);

        gameMan = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        currentSelectionCostume = gameMan.costumeSelection;
        currentSelectionColour = gameMan.colourSelection;
    }

    public void UnlockPiece()
    {
        for (int i = 0; i < cosmeticsets[currentSelectionCostume].pieceCost.Count; i++)
        {
            if (gameMan.currency >= cosmeticsets[currentSelectionCostume].pieceCost[i])
            {
                if (cosmeticsets[currentSelectionCostume].pieceUnlocked[i] == true)
                {

                }
                else if (cosmeticsets[currentSelectionCostume].basicCosmetic == true)
                {
                    gameMan.currency -= cosmeticsets[currentSelectionCostume].pieceCost[i];
                    cosmeticsets[currentSelectionCostume].pieceUnlocked[i] = true;
                    UpdateCostume();
                    break;
                }
                else if (cosmeticsets[currentSelectionCostume].basicCosmetic == false)
                {
                    gameMan.currency -= cosmeticsets[currentSelectionCostume].pieceCost[i];
                    cosmeticsets[currentSelectionCostume].pieceUnlocked[i] = true;
                    if (!cosmeticsets[currentSelectionCostume].pieceUnlocked.Contains(false))
                        cosmeticsets[currentSelectionCostume].cosmeticEffectActive = true;
                    // if (i == cosmeticsets[currentSelectionCostume].pieceUnlocked.Count - 2)
                    //{
                    //  gameMan.canProgressCosmeticMission = true;
                    //cosmeticsets[currentSelectionCostume].missionActive = true;
                    //}
                    UpdateCostume();
                    //NOTE calls the analytics and passes the currency you had, and how much your paid
                    Analytics.PurchaseCosmetic(cosmeticsets[currentSelectionCostume].pieceCost[i], gameMan.currency);
                    break;
                }
                else
                {
                    if (cosmeticsets[currentSelectionCostume].basicCosmetic == false)
                    {
                        //gameMan.canProgressCosmeticMission = true;
                        //cosmeticsets[currentSelectionCostume].missionActive = true;
                        UpdateCostume();
                        //Debug.Log("have to do mission");
                    }
                }
            }
            ActiveConfig.Save();
        }

        /* else if (cosmeticsets[currentSelectionCostume].missionActive == true)
         {
             cosmeticsets[currentSelectionCostume].missionProgress++;
             int i = cosmeticsets[currentSelectionCostume].pieceCost.Count - 1;
             if (cosmeticsets[currentSelectionCostume].missionProgress >= cosmeticsets[currentSelectionCostume].pieceCost[i])
             {
                 cosmeticsets[currentSelectionCostume].cosmeticEffectActive = true;
                 cosmeticsets[currentSelectionCostume].missionActive = false;
                 UpdateCostume();
             }
         }*/
    }

    //this is used for basic changes i.e. colour/hat
    public void UpdateColour()
    {
        //        FeedDataAndSave();
        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        }
        for (int i = 0; i < basicColour.Count; i++)
        {
            if (basicColour[i].isActive == true)
            {
                player.GetComponentInChildren<SpriteRenderer>().sprite = basicColour[i].newColour;
            }
        }
        ActiveConfig.Save();
    }

    //this updates the costume on the player
    public void UpdateCostume()
    {
        //   FeedDataAndSave();
        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        }
        Sprite cosmetic;

        for (int i = 0; i < cosmeticsets.Count; i++)
        {
            if (cosmeticsets[i].isActive == true)
            {
                if (cosmeticsets[i].CostumeSprites.Count < player.cosmetics.Count)
                {
                    int dif = 3;
                    for (int m = 0; m < player.cosmetics.Count; m++)
                    {
                        player.cosmetics[dif].sprite = null;
                        dif--;
                    }
                }
                for (int n = 0; n < cosmeticsets[i].CostumeSprites.Count; n++)
                {
                    //This will replace the costume sprite
                    if (cosmeticsets[i].CostumeSprites[n] != null)
                    {
                        cosmetic = cosmeticsets[i].CostumeSprites[n];
                        player.cosmetics[n].sprite = cosmetic;
                        if (cosmeticsets[i].pieceUnlocked[n] == true)
                        {
                            player.cosmetics[n].color = Color.white;
                        }
                        else
                        {
                            player.cosmetics[n].color = Color.black;
                        }
                    }
                    else
                    {
                        player.cosmetics[n].sprite = null;
                    }
                }
            }
        }
        ActiveConfig.Save();
    }
}