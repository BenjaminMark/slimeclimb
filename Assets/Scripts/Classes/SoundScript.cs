﻿using UnityEngine.Audio;
using UnityEngine;

[System.Serializable]
public class SoundScript  
{
    [Tooltip("name of sfx, this will be used to call it in code")]
    public string name;
    [Tooltip("Drag the audio clip you wish to play here")]
    public AudioClip clip;

    [Tooltip("controls the volume")]
    [Range(0f,1f)]
    public float volume;

    public bool randomisePitch;
    [Range(0f, 1f)]
    public float pitch;

    public bool loop; 

    //was gonna try and implement this but messing with the pitch isnt a good idea. 
   // [Range(.1f,.3f)]
    //public float pitch;

    [HideInInspector]
    public AudioSource source; 
	
}
