﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnThingOff : MonoBehaviour
{

    GameObject player;
    GameManager gameMan;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        gameMan = GameObject.FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player.transform.position != new Vector3(0, 0, 0) || gameMan.canPlay == false)
            gameObject.SetActive(false);
    }
}
