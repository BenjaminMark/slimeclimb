﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class Buttons : MonoBehaviour
{
    public GameObject splashPanel,
        MenuPanel,
        gameOverPanel,
        highScorePanel,
        chestButt;

    public Text currency;

    public AudioManager SFXManager;
    public AudioClip SFX;

    public Cosmetics cosmetics;
    public GameManager gameMan;
    public SelectCosmetics selectCos;
    public Chest chest;
    public Missions missionMan;
    public UI ui;
    public Lava lava;

    public bool chestsPresent;

    // ------- Click Variants (Riley) p.s. sorry for ugly programming

    public GameObject purchaseVar, clickAnim, playButton, cosmeticsButton;

    public List<Button> unlocks = new List<Button>();
    public void Start()
    {
        SFXManager = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
        gameMan = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        cosmetics = GameObject.FindGameObjectWithTag("CosmeticsManager").GetComponent<Cosmetics>();
        if (gameMan.chestKeys >= 1)
            chest = GameObject.FindGameObjectWithTag("ChestManager").GetComponent<Chest>();


    }

    public void UnlockCosmetic()
    {
        PurchaseVariant();
        ButtonClick();
        cosmetics.UnlockPiece();
        selectCos.UpdateSelectionSprites(selectCos.currentSelectionCostume);
        gameMan.GetPlayerCosmetics();
        selectCos.UpdateSelectionSprites(selectCos.currentSelectionCostume);
        currency.text = gameMan.currency.ToString();
        if (cosmetics.cosmeticsets[cosmetics.currentSelectionCostume].pieceUnlocked.Contains(true))
        {
            selectCos.coinPrice.text = ("Unlocked");
            selectCos.coinPrice.color = Color.grey;
        }
        //gameMan.FeedDataAndSave();
    }

    public void Load(int SceneNumber)
    {
        //PlayButtonClick();
        gameMan.FeedDataAndSave();
        cosmetics.ActiveConfig.Save();
        FindObjectOfType<AudioManager>().Play("buttonPress");
        Time.timeScale = 1;
        Analytics.OpenScreen(SceneNumber.ToString());
        SceneManager.LoadScene(SceneNumber);
        gameMan.AddRun();
    }

    public void CanPlay(bool canPlay)
    {
        gameMan.canPlay = canPlay;
    }

    public void UpdateCosmetic()
    {
        cosmetics.UpdateCostume();
    }

    public void ActivateMenu()
    {
        splashPanel.SetActive(false);
        MenuPanel.SetActive(true);
        //  SFXManager.Sounds("buttonPress");

    }

    public void ActivateHighScore()
    {
        highScorePanel.SetActive(true);
        gameOverPanel.SetActive(false);
        //SFXManager.Sounds("buttonPress");
    }

    public void ChestButton()
    {
        chest.DeterminePrize(chest.RandomiseLootReturn());
        //FindObjectOfType<AudioManager>().Play("chestOpen");
        FindObjectOfType<AudioManager>().Play("chestUnlock");
        FindObjectOfType<AudioManager>().Play("Prize");
        if (gameMan.chestKeys > 0)
        {
            gameMan.chestKeys--;
            //---------more things happen here for visuals----------


            //------------------------------------------------------
            if (gameMan.chestKeys == 0)
            {
                chestButt.SetActive(false);
            }
        }
        gameMan.FeedDataAndSave();
    }

    public void HeightMissionButton()
    {
        if (!missionMan)
            missionMan = GameObject.FindGameObjectWithTag("MissionManager").GetComponent<Missions>();
        missionMan.CheckMissionComplete(ref gameMan.heightProgress,
                missionMan.HeightMissions[missionMan.currentHeightMission].goal,
                ref missionMan.currentHeightMission,
                ref gameMan.heightMission,
                missionMan.HeightMissions,
                missionMan.HeightMissions[missionMan.currentHeightMission].coinReward,
                ref gameMan.canProgressHeightMission);
        if (!ui)
            ui = GameObject.FindGameObjectWithTag("UI").GetComponent<UI>();
        //ui.UpdateMissionUI(ui.HeightBar, gameMan.heightProgress, missionMan.HeightMissions[missionMan.currentHeightMission].goal);
        ui.UpdateGameOverUI();
        gameMan.FeedDataAndSave();
    }

    public void CoinMissionButton()
    {
        if (!missionMan)
            missionMan = GameObject.FindGameObjectWithTag("MissionManager").GetComponent<Missions>();
        // if (gameMan.canProgressCosmeticMission)
        //{
        //ui.UpdateMissionUI(ui.CoinBar, gameMan.cosmeticProgress, cosmetics.cosmeticsets[cosmetics.currentSelectionCostume].pieceCost[3]);
        //  ui.UpdateGameOverUI();
        //}


        missionMan.CheckMissionComplete(ref gameMan.coinProgress,
                missionMan.CoinMissions[missionMan.currentCoinMission].goal,
                ref missionMan.currentCoinMission,
                ref gameMan.coinMission,
                missionMan.CoinMissions,
                missionMan.CoinMissions[missionMan.currentCoinMission].coinReward,
                ref gameMan.canProgressCoinMission);
        if (!ui)
            ui = GameObject.FindGameObjectWithTag("UI").GetComponent<UI>();
        //ui.UpdateMissionUI(ui.CoinBar, gameMan.coinProgress, missionMan.CoinMissions[missionMan.currentCoinMission].goal);
        ui.UpdateGameOverUI();
        gameMan.FeedDataAndSave();

    }

    public void BoostMissionButton()
    {
        if (!missionMan)
            missionMan = GameObject.FindGameObjectWithTag("MissionManager").GetComponent<Missions>();
        missionMan.CheckMissionComplete(ref gameMan.boostProgress,
                missionMan.BoostMissions[missionMan.currentBoostMission].goal,
                ref missionMan.currentBoostMission,
                ref gameMan.boostMission,
                missionMan.BoostMissions,
                missionMan.BoostMissions[missionMan.currentBoostMission].coinReward,
                ref gameMan.canProgressBoostMission);
        if (!ui)
            ui = GameObject.FindGameObjectWithTag("UI").GetComponent<UI>();
        //ui.UpdateMissionUI(ui.BoostBar, gameMan.boostProgress, missionMan.BoostMissions[missionMan.currentBoostMission].goal);
        ui.UpdateGameOverUI();
        gameMan.FeedDataAndSave();
    }

    public void Pause()
    {
        if (!ui)
            ui = GameObject.FindGameObjectWithTag("UI").GetComponent<UI>();
        if (gameMan.playingAd == false)
        {
            gameMan.canPlay = false;
            ui.Activate(ui.pauseMenu);
            ui.Activate(ui.pausePlayButton);
        }
        gameMan.FeedDataAndSave();
    }

    public void PlayPause()
    {
        StartCoroutine(CountIn());
    }

    public IEnumerator CountIn()
    {
        ui.countDown.gameObject.SetActive(true);
        ui.pausePlayButton.gameObject.SetActive(false);
        ui.countDown.text = ("3");
        yield return new WaitForSeconds(1);
        ui.countDown.text = ("2");
        yield return new WaitForSeconds(1);
        ui.countDown.text = ("1");
        yield return new WaitForSeconds(1);
        ui.countDown.gameObject.SetActive(false);
        ui.pauseMenu.SetActive(false);
        gameMan.canPlay = true;
    }

    public void PurchaseVariant()
    {
        purchaseVar.GetComponent<Animator>().SetTrigger("Clicked");
    }

    public void ButtonClick()
    {
        clickAnim.GetComponent<Animator>().SetTrigger("Clicked");
    }

    public void PlayButtonClick()
    {
        playButton.GetComponentInChildren<Animator>().SetTrigger("Clicked");
    }

    public void CosmeticsButtonClick()
    {
        cosmeticsButton.GetComponent<Animator>().SetTrigger("Clicked");
    }
}
