﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    public GameManager gameMan;
    public AudioManager aud;
    public Vector3 loc;
    public Animator anim;
    public GameObject cosmeticEffect;
    public bool inputActive = true;

    public SpriteRenderer[] cosSprites;

    public TesterForPlayerMovement test;

    public float speed,
        step;

    public int failCount;

    //PLAYER INFO ---- COSTUME
    public List<SpriteRenderer> cosmetics = new List<SpriteRenderer>();


    // Use this for initialization
    void Start()
    {
        gameMan = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        failCount = 0;
    }

    public void ChangeSpriteAlpha(Color from, Color to)
    {
        foreach (SpriteRenderer cos in cosSprites)
        {
            if (cos.color == from)
            {
                cos.color = to;
            }
        }
    }
    public void Awake()
    {
        //NOTE
        //HACK only place I could realise that would be able to run on start of each round

    }

    // Update is called once per frame
    void Update()
    {
        step = speed * Time.deltaTime;
        if (gameMan.canPlay == true)
        {
            if (inputActive == true)
            {
                //Move(loc, 0.25f);
                if (anim.GetCurrentAnimatorStateInfo(0).IsName("IdleSlime") || anim.GetCurrentAnimatorStateInfo(0).IsName("IdleSlimeL"))
                {
                    anim.speed = 1;
                }
                else
                {
                    anim.speed = speed * 0.5f;
                }

                if (gameObject.transform.position != loc)
                {
                    Move(loc, step);
                }

                Jump(false, false);

                if (Input.touchCount == 1)
                {
                    foreach (Touch touch in Input.touches)
                    {
                        if (touch.position.x < Screen.width / 2)
                        {
                            if (touch.phase == TouchPhase.Began)
                            {
                                if (AdController.canSetStartTime())
                                {
                                    AdController.SetRoundStartTime();
                                }

                                test.play = false;
                                inputActive = false;
                                Jump(true, false);

                            }
                        }
                        else if (touch.position.x > Screen.width / 2)
                        {
                            if (touch.phase == TouchPhase.Began)
                            {
                                if (AdController.canSetStartTime())
                                {
                                    AdController.SetRoundStartTime();
                                }

                                test.play = false;
                                inputActive = false;
                                Jump(false, true);

                            }
                        }
                    }
                }
            }
            else
            {
                Move(loc, step);
            }

        }
        else
        {
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("IdleSlime") || anim.GetCurrentAnimatorStateInfo(0).IsName("IdleSlimeL"))
            {
                anim.speed = 1;
            }
        }

    }

    void Move(Vector3 newLoc, float time)
    {
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, newLoc, time);
    }

    void Jump(bool left, bool right)
    {
        if (Input.GetKeyDown(KeyCode.RightArrow) || right == true)
        {
            //gameObject.transform.position = transform.position + new Vector3(1, 1, 0);
            //gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, transform.position + new Vector3(1, 1, 0), 1);
            //gameMan.ScoreUpdate();
            //print("Right");
            //float rng = Random.Range(0, 1);
            anim.speed = speed * 0.5f;
            test.play = false;
            inputActive = false;
            anim.SetTrigger("JumpR");
            loc += new Vector3(1, 1, 0);
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow) || left == true)
        {
            //gameObject.transform.position = transform.position + new Vector3(-1, 1, 0);
            //gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, transform.position + new Vector3(-1, 1, 0), 1);
            //gameMan.ScoreUpdate();
            //print("Left");
            //float rng = Random.Range(0, 1);
            anim.speed = speed * 0.5f;
            test.play = false;
            inputActive = false;
            anim.SetTrigger("JumpL");
            loc += new Vector3(-1, 1, 0);
        }
    }

    public IEnumerator Fall(float wait)
    {
        anim.SetTrigger("fall");
        inputActive = false;
        loc -= new Vector3(0, 2, 0);
        yield return new WaitForSeconds(wait);
        inputActive = true;
    }
}
