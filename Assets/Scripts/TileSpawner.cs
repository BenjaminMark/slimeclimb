﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSpawner : MonoBehaviour
{
    public GameObject tilePrefab,
        lastTile;
    public Transform tileLoc;
    public GameManager gameMan;
    public int x;
    public bool lvlSpawner;
    public List<GameObject> tilesToDelete = new List<GameObject>();

    public float rng1, rng2;

    private void Start()
    {
        gameMan = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<GameManager>();
    }

    public void SpawnTiles(float x, float y, int repeat)
    {
        if(tilesToDelete.Count > 20)
        {
            int deleteamount = 20;
            for(int i = 0; i < deleteamount; i++)
            {
                tilesToDelete.Remove(tilesToDelete[0]);
            }
        }
        float xLoc = tileLoc.position.x - x;
        float yLoc = tileLoc.position.y + y;
        for (int i = 0; i < repeat; i++)
        {
            GameObject newTile = ObjectPooler.instance.GetPooledObject(); // Instantiate(tilePrefab);
            newTile.SetActive(true);

            Transform parent = newTile.transform;
            
            for(int n = 0; n < parent.childCount; n++)
            {
                Transform child = parent.GetChild(n);
                if(child.tag == "Boost")
                {
                    Debug.Log("Worked");
                    Destroy(child.gameObject);
                }
            }

            newTile.transform.position = new Vector3(xLoc, yLoc, 0);
            Tile newTileScript = newTile.GetComponent<Tile>();
            Animator anim = newTile.GetComponent<Animator>();
            anim.ResetTrigger("LightUp");
            anim.ResetTrigger("BadTile");
            anim.ResetTrigger("BlankTile");
            anim.ResetTrigger("GoldenPath");
            anim.ResetTrigger("BreakTile");
            anim.ResetTrigger("Reset");
            anim.ResetTrigger("Hide");
            anim.SetTrigger("Reset");
            //anim.Play("tileNeutral", -1, 0f);
            xLoc += 2;
            if (lastTile == null)
            {
                lastTile = newTile;
                newTileScript.AssignTileType(true, rng1, rng2);
            }
            else
            {
                if (lastTile.GetComponent<Tile>().tileType == Tile.TileTypes.BadTile || 
                    lastTile.GetComponent<Tile>().tileType == Tile.TileTypes.BlankTile)
                {
                    newTileScript.tileType = Tile.TileTypes.StandardTile;
                    newTileScript.AssignTileType(false, rng1, rng2);

                    lastTile = newTile;
                }
                else
                {
                   // newTileScript.tileType = Tile.TileTypes.BadTile;
                    newTileScript.AssignTileType(true, rng1, rng2);
                    lastTile = newTile;
                }
            }
            tilesToDelete.Add(newTile);
        }
    }

    public void DeleteTiles()
    {
        foreach(GameObject tile in tilesToDelete)
        {
            tile.SetActive(false);
        }
    }

}
