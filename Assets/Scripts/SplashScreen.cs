﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour
{
    [Header("Timer to display splash screen for x seconds")]
    public float timer;
    [Header("Scene index you want to load after splash screen is loaded ")]
    public int sceneToLoad = 4;
    public enum LoadingState
    {
        Start,
        Loading,
        Waiting,
        Done,
    }

    public LoadingState currentLoadState;
    AsyncOperation holdOnSplash;

    // Use this for initialization
    void Start()
    {
        //HACK seems to be half the time it needs to be 
        timer = timer * 2;
        currentLoadState = LoadingState.Start;
        StartCoroutine(AsyncStart(sceneToLoad));
    }

    IEnumerator AsyncStart(int sceneIndex)
    {
        holdOnSplash = SceneManager.LoadSceneAsync(sceneIndex);
        holdOnSplash.allowSceneActivation = false;

        yield return new WaitForSecondsRealtime(timer);
        //Debug.Log("timer worked");
        holdOnSplash.allowSceneActivation = true;
        yield return null;
    }
}