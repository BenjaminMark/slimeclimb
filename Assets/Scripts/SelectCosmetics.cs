﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectCosmetics : MonoBehaviour
{

    //#region SaveLoading Code
    //[SerializeField] private GameConfig ReferenceConfig; // this links to the SO

    //public GameConfig ActiveConfig;

    //// Update is called once per frame
    //public void FeedDataAndSave()
    //{
    //    //somehow save that is cosmetic active or unlocked :/



    //    ActiveConfig.Save();
    //}
    ///// <summary>
    ///// This is so bad but neeed to get this workinggsljfhaslkjdfhalkjs
    ///// </summary>
    //public void LoadAndPullData()
    //{
    //    ActiveConfig.Load();
    //    if (ActiveConfig.savedIntergers.Count > 0)
    //    {
    //        //somehow load that is cosmetic active or unlocked :/
    //    }

    //}

    //void Awake()
    //{
    //    // clone the reference config so any changes won't overwrite
    //    ActiveConfig = ScriptableObject.Instantiate(ReferenceConfig);
    //    ActiveConfig.Load();
    //}
    //#endregion


    public Text costumeName;

    public List<Image> costumeSprites = new List<Image>();

    public Cosmetics cosmeticsData;

    public GameManager gameMan;

    public GameObject crystal, rightClickAnim, leftClickAnim;

    public Text coinPrice,
        currency;

    public int currentSelectionCostume,
        currentSelectionColour;

    // Use this for initialization
    void Start()
    {
        cosmeticsData = GameObject.FindGameObjectWithTag("CosmeticsManager").GetComponent<Cosmetics>();
        gameMan = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        gameMan.canPlay = false;
        gameMan.ReplacePlayerCosmetics();
        cosmeticsData.currentSelectionCostume = gameMan.costumeSelection;
        cosmeticsData.currentSelectionColour = gameMan.colourSelection;
        currentSelectionCostume = cosmeticsData.currentSelectionCostume;
        costumeName.text = cosmeticsData.ActiveConfig.cosmeticsets[currentSelectionCostume].name;
        currentSelectionColour = cosmeticsData.currentSelectionColour;
        if (currency)
        {
            currency.text = gameMan.currency.ToString();
            coinPrice.text = cosmeticsData.ActiveConfig.cosmeticsets[currentSelectionCostume].pieceCost[0].ToString();
            coinPrice.color = Color.white;
            if (cosmeticsData.ActiveConfig.cosmeticsets[currentSelectionCostume].pieceUnlocked.Contains(true))
            {
                coinPrice.color = Color.grey;
                coinPrice.text = ("Unlocked");
            }
        }
        cosmeticsData.UpdateCostume();
        UpdateSelectionSprites(currentSelectionCostume);
    }

    public void CostumeRight()
    {
        
        cosmeticsData.ActiveConfig.cosmeticsets[currentSelectionCostume].isActive = false;
        currentSelectionCostume++;
        if (currentSelectionCostume > cosmeticsData.cosmeticsets.Count - 1)
        {
            currentSelectionCostume = 0;
            cosmeticsData.currentSelectionCostume = currentSelectionCostume;
        }
        cosmeticsData.ActiveConfig.cosmeticsets[currentSelectionCostume].isActive = true;
        cosmeticsData.UpdateCostume();
        //cosmeticsData.ActiveConfig.cosmeticsets[currentSelectionCostume].isActive = false;
        cosmeticsData.currentSelectionCostume = currentSelectionCostume;
        gameMan.costumeSelection = currentSelectionCostume;
        coinPrice.text = cosmeticsData.ActiveConfig.cosmeticsets[currentSelectionCostume].pieceCost[0].ToString();
        coinPrice.color = Color.white;

        if (cosmeticsData.ActiveConfig.cosmeticsets[currentSelectionCostume].pieceUnlocked.Contains(true))
        {
            cosmeticsData.ActiveConfig.cosmeticsets[currentSelectionCostume].isActive = true;
            cosmeticsData.currentSelectionCostume = currentSelectionCostume;
            gameMan.costumeSelection = currentSelectionCostume;
            cosmeticsData.ActiveConfig.Save();
            coinPrice.color = Color.grey;
            coinPrice.text = ("Unlocked");
        }

        costumeName.text = cosmeticsData.cosmeticsets[currentSelectionCostume].name;

        Rotate rot = FindObjectOfType<Rotate>();
        rot.CoinClick();
        rot.CoinClick();
        UpdateSelectionSprites(currentSelectionCostume);
        cosmeticsData.ActiveConfig.Save();
        gameMan.GetPlayerCosmetics();
        gameMan.FeedDataAndSave();
        RightButtonClick();
    }

    public void CostumeLeft()
    {

        cosmeticsData.ActiveConfig.cosmeticsets[currentSelectionCostume].isActive = false;
        currentSelectionCostume--;
        if (currentSelectionCostume < 0)
        {
            currentSelectionCostume = cosmeticsData.ActiveConfig.cosmeticsets.Count - 1;
            cosmeticsData.currentSelectionCostume = currentSelectionCostume;
        }
        cosmeticsData.ActiveConfig.cosmeticsets[currentSelectionCostume].isActive = true;
        cosmeticsData.UpdateCostume();
        //cosmeticsData.ActiveConfig.cosmeticsets[currentSelectionCostume].isActive = false;
        cosmeticsData.currentSelectionCostume = currentSelectionCostume;
        gameMan.costumeSelection = currentSelectionCostume;
        coinPrice.text = cosmeticsData.ActiveConfig.cosmeticsets[currentSelectionCostume].pieceCost[0].ToString();
        coinPrice.color = Color.white;

        if (cosmeticsData.ActiveConfig.cosmeticsets[currentSelectionCostume].pieceUnlocked.Contains(true))
        {
            cosmeticsData.ActiveConfig.cosmeticsets[currentSelectionCostume].isActive = true;
            cosmeticsData.currentSelectionCostume = currentSelectionCostume;
            gameMan.costumeSelection = currentSelectionCostume;
            cosmeticsData.ActiveConfig.Save();
            coinPrice.color = Color.grey;
            coinPrice.text = ("Unlocked");
        }

        costumeName.text = cosmeticsData.cosmeticsets[currentSelectionCostume].name;


        Rotate rot = FindObjectOfType<Rotate>();
        rot.CoinClick();
        rot.CoinClick();
        UpdateSelectionSprites(currentSelectionCostume);
        cosmeticsData.ActiveConfig.Save();
        gameMan.GetPlayerCosmetics();
        gameMan.FeedDataAndSave();
        LeftButtonClick();
    }

    public void ColourDown()
    {
        cosmeticsData.ActiveConfig.basicColour[currentSelectionColour].isActive = false;
        currentSelectionColour--;
        if (currentSelectionColour < 0)
        {
            currentSelectionColour = cosmeticsData.ActiveConfig.basicColour.Count - 1;
        }
        cosmeticsData.ActiveConfig.basicColour[currentSelectionColour].isActive = true;
        gameMan.colourSelection = currentSelectionColour;
        cosmeticsData.UpdateColour();
        gameMan.GetPlayerCosmetics();
        FindObjectOfType<AudioManager>().Play("crystalTink");
        cosmeticsData.ActiveConfig.Save();
        gameMan.FeedDataAndSave();
    }

    public void UpdateSelectionSprites(int activeSet)
    {
        if (cosmeticsData.ActiveConfig.cosmeticsets[activeSet].CostumeSprites.Count < costumeSprites.Count)
        {
            int dif = 3;
            for (int m = 0; m < costumeSprites.Count; m++)
            {
                costumeSprites[dif].color = Color.clear;
                dif--;
            }
        }
        for (int i = 0; i < cosmeticsData.ActiveConfig.cosmeticsets[activeSet].CostumeSprites.Count; i++)
        {
            if (cosmeticsData.ActiveConfig.cosmeticsets[activeSet].CostumeSprites[i] != null)
            {
                Sprite spr = cosmeticsData.ActiveConfig.cosmeticsets[activeSet].CostumeSprites[i];
                costumeSprites[i].sprite = spr;
                if (cosmeticsData.ActiveConfig.cosmeticsets[activeSet].pieceUnlocked[i] == false)
                {
                    costumeSprites[i].color = Color.black;
                }
                else
                {
                    costumeSprites[i].color = Color.white;
                }
            }
            else
            {
                costumeSprites[i].sprite = null;
            }
        }
    }

    public void TapCrystal()
    {
        crystal.GetComponent<Animator>().SetTrigger("Tapped");
    }

    public void RightButtonClick()
    {
        rightClickAnim.GetComponent<Animator>().SetTrigger("Clicked");
    }

    public void LeftButtonClick()
    {
        leftClickAnim.GetComponent<Animator>().SetTrigger("Clicked");
    }


}
