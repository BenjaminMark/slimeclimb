﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LavaDistanceMetre : MonoBehaviour
{

    float dist;
    public Slider slider;
    public GameObject player,
        lava;

    // Update is called once per frame
    void Update()
    {
        dist = Vector3.Distance(player.transform.position, lava.transform.position) * 0.1f;
        slider.value = dist;
    }
}
