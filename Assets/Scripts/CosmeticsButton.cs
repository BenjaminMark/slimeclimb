﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CosmeticsButton : MonoBehaviour
{

    public Buttons buttons;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadSceneOnClick()
    {
        buttons.CanPlay(false);
        buttons.Load(2);
    }
}
