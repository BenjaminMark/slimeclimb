﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaFreeze : MonoBehaviour
{

    public Lava lava;
    public BoostSpawner bstSpawner;

    // Use this for initialization
    void Start()
    {
        lava = GameObject.FindGameObjectWithTag("Lava").GetComponent<Lava>();
        bstSpawner = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<BoostSpawner>();        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            lava.move = false;
            gameObject.GetComponentInChildren<SpriteRenderer>().color = Color.clear;
            SpriteRenderer[] sprites = lava.gameObject.GetComponentsInChildren<SpriteRenderer>();
            for (int i = 0; i < sprites.Length; i++)
            {
                sprites[i].color = Color.blue;
            }
            StartCoroutine(RunDeathTimer(3));
        }
    }

    IEnumerator RunDeathTimer(float timer)
    {
        yield return new WaitForSeconds(timer);
        bstSpawner.boostActive = false;
        lava.move = true;
        SpriteRenderer[] sprites = lava.gameObject.GetComponentsInChildren<SpriteRenderer>();
        for (int i = 0; i < sprites.Length; i++)
        {
            sprites[i].color = Color.white;
        }
        Destroy(gameObject);
    }
}
