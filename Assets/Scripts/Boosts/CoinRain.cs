﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinRain : MonoBehaviour
{
    //bool pickedUp = false;
    public GameObject coin, 
        lPoint,
        rPoint;
    public BoostSpawner bstSpawner;
    public Collider2D col1;
    public float deathTimer;
    public bool makeItRain = false;
    public float timer;
    public Animator boostAnim; 

    // Use this for initialization
    void Start()
    {
        boostAnim = gameObject.GetComponent<Animator>();
        bstSpawner = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<BoostSpawner>();
    }

    public void Update()
    {
        if(makeItRain == true)
        {
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                CoinSpawn();
                timer = Random.Range(0, 0.1f);
            }
        }
    }

    public void CoinSpawn()
    {
        float x = Random.Range(lPoint.transform.position.x, rPoint.transform.position.x);
        GameObject c = Instantiate(coin);
        c.GetComponent<Rigidbody2D>().gravityScale = 1;
        c.transform.position = new Vector3(x, lPoint.transform.position.y);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {            
            //pickedUp = true;
            FindObjectOfType<AudioManager>().Play("coinRain");
            boostAnim.SetTrigger("lightning");
            gameObject.GetComponentInChildren<SpriteRenderer>().color = Color.clear;
            gameObject.transform.parent = collision.gameObject.transform;
            gameObject.transform.position = collision.gameObject.transform.position;
            col1.enabled = false;
            bstSpawner.boostActive = true;
            makeItRain = true;
            timer = 0.1f;
            StartCoroutine(RunDeathTimer(deathTimer));
        }

        /*if (collision.gameObject.CompareTag("GoodTile") || collision.gameObject.CompareTag("BadTile"))
        {
            if (pickedUp == true)
            {                
                int rngAmount = Random.Range(0, 4);
                for (int i = 0; i < rngAmount; i++)
                {
                    float maxX = 0.5f;
                    float minX = -0.5f;
                    float rngPlacement = Random.Range(minX, maxX);
                    Vector3 offset = new Vector3(rngPlacement, 0.5f, 0);
                    Instantiate(coin, collision.gameObject.transform.position + offset, gameObject.transform.rotation);
                }
            }
        }*/
    }

    IEnumerator RunDeathTimer(float timer)
    {
        yield return new WaitForSeconds(timer);
        bstSpawner.boostActive = false;
        Destroy(gameObject);
    }
}
