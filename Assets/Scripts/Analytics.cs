﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public static class Analytics
{
    public static string storedAnalytics = "isCurrentltyData";
    public static string timePlayedKey = "timePlayed";
    public static string runsPlayedKey = "totalRuns";

    public static void OpenScreen(string screenName)
    {
        AnalyticsEvent.ScreenVisit(screenName);
    }
    public static void GameStart(bool happen)
    {
        if (!Application.isEditor)
        {
            AnalyticsEvent.LevelStart("GameStarted", new Dictionary<string, object>()
            {

            { "DidGameStart?", happen }

            }); 
        }

        else
        {
//            Debug.LogFormat("Game Started Analytics. {0}", happen);
        }
    }

    /// <summary>
    /// Triggered at the end of the run. sends custom event to Analytics with the score and the coins of the current run
    /// </summary>
    /// <param name="score">The score earned over the run</param>
    /// <param name="coins"> the amount of coins collected during the run</param>
    public static void RoundEnd(int score, int coins)
    {
        if (!Application.isEditor)
        {
            AnalyticsEvent.Custom("UserDeath", new Dictionary<string, object>()
                {
                    { "Score", score },
                    { "Coins", coins }
                }
                );
        }
        else
        {
//            Debug.LogFormat("Round End score is {0}, and coins collected was {1}", score, coins);
        }
    }

    public static void PurchaseCosmetic(int cost, int currency)
    {
        if(!Application.isEditor)
        {
            AnalyticsEvent.Custom("Cosmetics", new Dictionary<string, object>()
            {
                { "Cost", cost },
                { "Currency", currency }
            });
        }
        else
        {
            Debug.LogFormat("Cosmetic Purchased for {0}, the player has {1} ", cost, currency);

        }
    }

    public static void GamePaused(float timePlayed, int runsPlayed)
    {
        if (!Application.isEditor)
        {
            AnalyticsEvent.Custom("Session Data", new Dictionary<string, object>()
            {
                { "Total Time Played", timePlayed},
                { "Total Runs Played", runsPlayed }
            });
        }
        else
        {
            Debug.LogFormat("Game Paused | Time of game session: {0} and runs played {1}", timePlayed, runsPlayed);

        }
    }
    public static void GameQuit(float timePlayed, int runsPlayed)
    {
        if (!Application.isEditor)
        {
            AnalyticsEvent.Custom("Session Data", new Dictionary<string, object>()
            {
                { "Total Time Played", timePlayed},
                { "Total Runs Played", runsPlayed }
            });
        }
        else
        {
            //Debug.LogFormat("Game quit | Time of game session: {0} and runs played {1}", timePlayed, runsPlayed);
        }
    }
}