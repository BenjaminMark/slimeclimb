﻿using System.Collections.Generic;
using UnityEngine;

public static class AdController
{
    public static int isFirstGameBool = 0;
    private static readonly string isFirstGame = "isFirstGame";

    public static Dictionary<int, bool> firstRunAdDictionary = new Dictionary<int, bool>()
    {
        {1, false },
        {2, false },
        {3, false },
        {4, true },
        {5, true },
        {6, false },
        {7, true }
    };
    public static Dictionary<int, bool> normalRunsAdDictionary = new Dictionary<int, bool>()
    {
        {1, false },
        {2, false },
        {3, true },
        {4, true },
        {5, false },
        {6, true },
        {7, false }
    };
    //The Selected dictionary defaults to the normanl use dic, in case there is issues assigning to it.
    private static Dictionary<int, bool> SelectedDic
    {
        get
        {
            if (isFirstGameBool == 0)
            {
                return firstRunAdDictionary;
            }
            else
            {
                return normalRunsAdDictionary;
            }
        }
    }

    private static float roundStartTime = 0;
    private static float roundEndTime = 0;
    //private static float currentRunTime = 0;
    private static bool isStartTimeSet = false;

    public static void SetFirstGameFlag()
    {
        isFirstGameBool = PlayerPrefs.GetInt(isFirstGame);
    }

    public static bool CanPlayAd(int currentRun)
    {
        switch (currentRun)
        {
            default:
                return DidRoundTakeLongerThanSixtySeconds();
            case 1:
                if (SelectedDic.ContainsKey(currentRun))
                {
                    return SelectedDic[currentRun];
                }

                break;
            case 2:
                if (SelectedDic.ContainsKey(currentRun))
                {
                    return SelectedDic[currentRun];
                }

                break;
            case 3:
                if (SelectedDic.ContainsKey(currentRun))
                {
                    return SelectedDic[currentRun];
                }

                break;
            case 4:
                if (SelectedDic.ContainsKey(currentRun))
                {
                    return SelectedDic[currentRun];
                }

                break;
            case 5:
                if (SelectedDic.ContainsKey(currentRun))
                {
                    return SelectedDic[currentRun];
                }

                break;
            case 6:
                if (SelectedDic.ContainsKey(currentRun))
                {
                    return SelectedDic[currentRun];
                }

                break;
            case 7:
                if (SelectedDic.ContainsKey(currentRun))
                {
                    return SelectedDic[currentRun];
                }

                break;
        }
        return false;
    }
    public static bool DidRoundTakeLongerThanSixtySeconds()
    {
        if ((roundEndTime - roundStartTime) >= 60 /*seconds*/)
        {
            Debug.LogFormat("Round took {0} seconds long", roundEndTime - roundStartTime);
            return true;
        }
        else
        {
            return false;
        }
    }
    public static void SetRoundStartTime()
    {
        roundStartTime = Time.time;
        isStartTimeSet = true;
    }
    public static void SetRoundEndTime()
    {
        isStartTimeSet = false;
        roundEndTime = Time.time;
    }
    public static bool canSetStartTime()
    {
        if(isStartTimeSet)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}