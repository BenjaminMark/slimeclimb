﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// put any common variable/data for ads here
public class AdCommon {
#if UNITY_IOS
       public static string GameId = "2926225";
#elif UNITY_ANDROID
    public static string GameId = "2926223";
#endif

    public static string FailAdPlacementId = "video";
}
